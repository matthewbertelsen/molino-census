module.exports = getUpdatedAggregates;

var R = require('ramda');

const propOr0 = R.propOr(0);
const propOrEmptyList = R.propOr([]);

function getUpdatedAggregates(aggregates, newResponse) {
  newResponse = newResponse || {};
  aggregates = aggregates || {};

  newResponse.fCount = propOrEmptyList('fAges', newResponse).length;
  newResponse.mCount = propOrEmptyList('mAges', newResponse).length;
  newResponse.populationCount = newResponse.fCount + newResponse.mCount;

   // gives both the same structure so they can be evolved
  const fullNewResponse = fillAggregateObj(newResponse);
  const fullAggregates = fillAggregateObj(aggregates);

  fullNewResponse.interestsAndComplaints.interestCounts
    = itemsToCounts(fullNewResponse.interests);

  fullNewResponse.interestsAndComplaints.complaintCounts
    = itemsToCounts(fullNewResponse.complaints);

  const mergeCountObjects = R.mergeWithKey(mergeCountObj);
  const transformations = {
    fCount: R.add,
    mCount: R.add,
    populationCount: R.add,
      // flip so new ages are appended instead of prepended
    fAges: R.flip(R.concat),
    mAges: R.flip(R.concat),
    interestsAndComplaints: {
      complaintCounts: mergeCountObjects,
      interestCounts: mergeCountObjects
    }
  };

  const newAggregates = R.pipe(
                          R.evolve(transformations),
                          R.evolve(R.__, fullAggregates),
                          R.omit(['interests', 'complaints'])
                        )(fullNewResponse);

  return newAggregates;
}

function mergeCountObj(key, left, right) {
  function mergeInnerCountObj(keyInner, leftInner, rightInner) {
    return keyInner === 'label' ? leftInner : leftInner + rightInner;
  }

  return R.mergeWithKey(mergeInnerCountObj)(left)(right);
}

function fillInterestsAndComplaints(obj) {
  if (obj.interestsAndComplaints) {
    return {
      interestCounts: obj.interestsAndComplaints.interestCounts || {},
      complaintCounts: obj.interestsAndComplaints.complaintCounts || {}
    };
  } else {
    return { interestCounts: {}, complaintCounts: {} };
  }
}

function fillAggregateObj(obj) {
  return {
    fCount: propOr0('fCount', obj),
    mCount: propOr0('mCount', obj),
    populationCount: propOr0('populationCount', obj),
    fAges: propOrEmptyList('fAges', obj),
    mAges: propOrEmptyList('mAges', obj),
    interestsAndComplaints: fillInterestsAndComplaints(obj),
    interests: propOrEmptyList('interests', obj),
    complaints: propOrEmptyList('complaints', obj)
  };
}

function itemsToCounts(items) {
  return R.indexBy(R.prop('label'), R.map(newItem, items));
}

function newItem(item) {
  return { label: item, count: 1 };
}

