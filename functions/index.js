const functions = require('firebase-functions');
const admin = require('firebase-admin');

const getUpdatedAggregates = require('./getUpdatedAggregates');

admin.initializeApp(functions.config().firebase);

exports.aggregateSurveyResponse = functions.database
  .ref('/survey_responses/{response}')
  .onWrite(updateAggregates);

function updateAggregates(event) {
  return admin.database()
           .ref('/aggregates/')
           .once('value')
           .then(function(snapshot) {
             const aggregates = snapshot.val();
             const newResponse = event.data.val();

             const updatedAggregates
               = getUpdatedAggregates(aggregates, newResponse);

             return admin.database()
                      .ref('/aggregates/')
                      .update(updatedAggregates);
           });
}

