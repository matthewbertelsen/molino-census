var test = require('tape');
var getUpdatedAggregates = require('./getUpdatedAggregates');

test('returns obj full of empties if passed 2 empty params', function(t) {
  var aggregates = {};
  var newResponse = {};

  var expected = {
    fCount: 0,
    mCount: 0,
    populationCount: 0,
    fAges: [],
    mAges: [],
    interestsAndComplaints: {
      interestCounts: {},
      complaintCounts: {}
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

test('returns obj full of empties if both params are null', function(t) {
  var aggregates = null;
  var newResponse = null;

  var expected = {
    fCount: 0,
    mCount: 0,
    populationCount: 0,
    fAges: [],
    mAges: [],
    interestsAndComplaints: {
      interestCounts: {},
      complaintCounts: {}
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

test('adds f/m/population counts when newResponse empty', function(t) {
  var aggregates = { fCount: 1, mCount: 1, populationCount: 1 };
  var newResponse = {};

  var expected = {
    fCount: 1,
    mCount: 1,
    populationCount: 1,
    fAges: [],
    mAges: [],
    interestsAndComplaints: {
      interestCounts: {},
      complaintCounts: {}
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

test('aggregates fAges/mAges when aggregates is empty', function(t) {
  var aggregates = {};
  var newResponse = { mAges: [18], fAges: [18] };

  var expected = {
    fCount: 1,
    mCount: 1,
    populationCount: 2,
    fAges: [18],
    mAges: [18],
    interestsAndComplaints: {
      interestCounts: {},
      complaintCounts: {}
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

test('aggregates fAges/mAges and counts when aggregates is not empty', function(t) {
  var aggregates = {
    fCount: 2,
    mCount: 3,
    populationCount: 5,
    fAges: [1, 1],
    mAges: [1, 1, 1]
  };
  var newResponse = { mAges: [18], fAges: [18] };

  var expected = {
    fCount: 3,
    mCount: 4,
    populationCount: 7,
    fAges: [1, 1, 18],
    mAges: [1, 1, 1, 18],
    interestsAndComplaints: {
      interestCounts: {},
      complaintCounts: {}
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

test('adds a new complaint', function(t) {
  var aggregates = {};
  var newResponse = { complaints: ['something'] };

  var expected = {
    fCount: 0,
    mCount: 0,
    populationCount: 0,
    fAges: [],
    mAges: [],
    interestsAndComplaints: {
      interestCounts: {},
      complaintCounts: {
        something: {
          label: 'something',
          count: 1
        }
      }
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

test('adds a new interest', function(t) {
  var aggregates = {};
  var newResponse = { interests: ['something'] };

  var expected = {
    fCount: 0,
    mCount: 0,
    populationCount: 0,
    fAges: [],
    mAges: [],
    interestsAndComplaints: {
      interestCounts: {
        something: {
          label: 'something',
          count: 1
        }
      },
      complaintCounts: {}
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

test('adds to an existing interest', function(t) {
  var aggregates = {
    interestsAndComplaints: {
      interestCounts: {
        something: {
          label: 'something',
          count: 2
        }
      }
    }
  };
  var newResponse = { interests: ['something'] };

  var expected = {
    fCount: 0,
    mCount: 0,
    populationCount: 0,
    fAges: [],
    mAges: [],
    interestsAndComplaints: {
      interestCounts: {
        something: {
          label: 'something',
          count: 3
        }
      },
      complaintCounts: {}
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

test('adds to an existing complaint', function(t) {
  var aggregates = {
    interestsAndComplaints: {
      complaintCounts: {
        something: {
          label: 'something',
          count: 2
        }
      }
    }
  };
  var newResponse = { complaints: ['something'] };

  var expected = {
    fCount: 0,
    mCount: 0,
    populationCount: 0,
    fAges: [],
    mAges: [],
    interestsAndComplaints: {
      interestCounts: {},
      complaintCounts: {
        something: {
          label: 'something',
          count: 3
        }
      }
    }
  };
  var actual = getUpdatedAggregates(aggregates, newResponse);
  t.deepEqual(actual, expected);
  t.end();
});

