# Molino Census

Web-app for surveying the population of my neighborhood as a part of the data strategy for community integration.

## Features

  - Survey page with custom UI components
  - Results page with real-time updates and custom visualizations
  - Authentication

## Stack

  - Language: ES2015
  - Framework: VueJS
  - Libraries: d3.js, ramda
  - Bundler: Webpack
  - Tests: Tape (unit), TestCafe (e2e)
  - Deployment: Firebase Database, Hosting and Cloud Functions

