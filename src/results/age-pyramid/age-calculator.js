import { max } from 'd3-array';
import { concat, countBy, length, map, pipe, range, reduce } from 'ramda';

export default getHistBins;

function getHistBins(agesBySex, binSize) {
  const fAges = agesBySex.f;
  const mAges = agesBySex.m;

  const allAges = concat(fAges, mAges);
  const maxAge = max(allAges);
  const numPeople = length(allAges);

  const getBinIndex = getBinIndexGetter(binSize);

  const countByBinIndex = countBy(getBinIndex);
  const divideAllByNumPeople = map(divideBy(numPeople));
  const getBinRelFreqs = pipe(
                           countByBinIndex,
                           divideAllByNumPeople
                         );

  const relFreqsBySex = {
      f: getBinRelFreqs(fAges),
      m: getBinRelFreqs(mAges)
    };

  const getBinObj = getBinObjGetter(relFreqsBySex);
  const binCount = getBinIndex(maxAge) + 1;
  const histBins = map(getBinObj, range(0, binCount, 1));

  return histBins;
}

function getBinObjGetter(relFreqsBySex) {
  function getBinObj(i) {
    return {
             index: i,
             f: relFreqsBySex.f[i] || 0,
             m: relFreqsBySex.m[i] || 0
           };
  }

  return getBinObj;
}

function getBinIndexGetter(binSize) {
  return (age) => Math.floor(age / binSize);
}

function divideBy(denominator) {
  return (numerator) => numerator / denominator;
}

