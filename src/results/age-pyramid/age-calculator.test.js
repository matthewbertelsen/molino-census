import test from 'tape-catch';

import histBinsFrom from './age-calculator';

test('age-pyramid/age-calculator', (t) => {
  t.test('- calculates relative frequencies correctly', (t) => {
    const agesBySex = { f: [3, 7], m: [3, 3] };

    const expectedBinData = [{ index: 0, f: 0.25, m: 0.5},
                             { index: 1, f: 0.25, m: 0}];
    const actualBinData = histBinsFrom(agesBySex, 5);

    t.deepEquals(actualBinData, expectedBinData);
    t.end();
  });

  t.test('- does not omit an age when on bin limit', (t) => {
    const binSize = 7;
    const agesBySex = { f: [binSize], m: [] };
    const expectedBin = 1;

    const bins = histBinsFrom(agesBySex, binSize);
    const placedIn2ndBin = bins[expectedBin].f === 1;

    t.true(placedIn2ndBin);
    t.end();
  });

  t.test('- does not omit bins when they are empty', (t) => {
    const binSize = 5;
    const maxAge = 20;
    const agesBySex = { f: [], m: [maxAge] };

    const expectedBinCount = 5;
    const actualBinCount = histBinsFrom(agesBySex, binSize).length;

    t.equals(actualBinCount, expectedBinCount);
    t.end();
  });
});

