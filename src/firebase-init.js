import * as firebase from 'firebase/app';

export default (function() {
  firebase.initializeApp({
    apiKey: "AIzaSyBoe6Umv5-Z4R_DfME0iRkcU4F0GeEMBBU",
    authDomain: "avelmolino-996e6.firebaseapp.com",
    databaseURL: "https://avelmolino-996e6.firebaseio.com",
    projectId: "avelmolino-996e6",
    storageBucket: "avelmolino-996e6.appspot.com",
    messagingSenderId: "214518387742"
  });

  return firebase;
})();

